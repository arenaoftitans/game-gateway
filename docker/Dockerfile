FROM golang:alpine3.7 AS gobuild

ADD https://github.com/golang/dep/releases/download/v0.4.1/dep-linux-amd64 /go/bin/dep
RUN chmod +x /go/bin/dep && \
    apk add --update git
COPY src/* Gopkg* /go/src/gitlab.com/arenaoftitans/game-gateway/
RUN cd /go/src/gitlab.com/arenaoftitans/game-gateway/ && \
    dep ensure && \
    CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' -o /go/bin/aot-game-gateway .

FROM alpine:3.7
ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.1/dumb-init_1.2.1_amd64 /usr/local/bin/dumb-init
COPY --from=gobuild /go/bin/aot-game-gateway /usr/local/bin

# Create user with which to run the API.
RUN adduser -S aot && \
    chmod +x /usr/local/bin/dumb-init
USER aot

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD [ "/usr/local/bin/aot-game-gateway" ]
