-include Makefile.in

SERVICE = aot-game-gateway
SOURCEDIR = ./src
SOURCES := $(shell find $(SOURCEDIR) -name '*.go' -not -path './vendor/*')
EXEC_FILE := bin/$(SERVICE)
GODEP_EXEC ?= godep
WATCHER_EXEC ?= watcher

.PHONY: build, run, deps, lint, dockerimage, watch

deps: Gopkg.lock Gopkg.toml
	$(GODEP_EXEC) ensure

lint:
	golint src

build: $(EXEC_FILE)

$(EXEC_FILE): $(SOURCES)
	mkdir -p bin
	go build -o bin/$(SERVICE) $(SOURCES)

run: build
	./bin/$(SERVICE)

watch:
	cd src; API_DEBUG=true SECRET=TEST_SECRET $(WATCHER_EXEC)

dockerimage:
ifdef VERSION
	docker build \
	    -f docker/Dockerfile \
	    -t "registry.gitlab.com/arenaoftitans/game-gateway:${VERSION}" \
	    .
	# Testing image
	docker run \
		-d \
		--rm \
		--name aot-game-gateway-test \
		"registry.gitlab.com/arenaoftitans/game-gateway:${VERSION}"
	sleep 10
	@if [ "$$(docker inspect aot-game-gateway-test -f '{{ .State.Status }}')" = 'running' ]; then \
		echo '***** Working *****'; \
	else \
		echo '***** Failed! *****'; \
		exit 1; \
	fi
	docker stop aot-game-gateway-test
	docker push "registry.gitlab.com/arenaoftitans/game-gateway:${VERSION}"
else
	@echo "You must supply VERSION"
	exit 1
endif
