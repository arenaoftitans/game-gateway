package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
)

type registerPayload struct {
	Name string `json:"name"`
	Secret string `json:"secret"`
}

type registrationInfoPayload struct {
	Name string `json:"name"`
}

type registrationResponse struct {
	Success bool     `json:"success"`
	Errors  []string `json:"errors"`
}

func registerLatestAPI(resp http.ResponseWriter, req *http.Request, gatewaySecret string) (name string, err error) {
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		log.Printf("Error reading body: %v", err)
		var buf bytes.Buffer
		json.NewEncoder(&buf).Encode(registrationResponse{Success: false, Errors: []string{"Can't read body"}})
		http.Error(resp, buf.String(), http.StatusBadRequest)
		return "", errors.New("Registration error")
	}

	var data registerPayload
	err = json.Unmarshal(body, &data)
	if err != nil || data.Name == "" {
		log.Printf("Error while decoding latest API registration: %v", err)
		log.Println("The new name may also be empty.")
		var buf bytes.Buffer
		json.NewEncoder(&buf).Encode(registrationResponse{Success: false, Errors: []string{"Can't decode JSON body"}})
		http.Error(resp, buf.String(), http.StatusBadRequest)
		return "", errors.New("Registration error")
	} else if gatewaySecret == "" || data.Secret == "" {
	    log.Println("API secret is not defined in the message or in the configuration. Cannot update registration.")
	    var buf bytes.Buffer
		json.NewEncoder(&buf).Encode(registrationResponse{Success: false, Errors: []string{"Secret is not defined"}})
		http.Error(resp, buf.String(), http.StatusBadRequest)
		return "", errors.New("Empty secret")
	} else if gatewaySecret != data.Secret {
	    log.Println("Supplied secret is invalid. Cannot update registration.")
	    var buf bytes.Buffer
		json.NewEncoder(&buf).Encode(registrationResponse{Success: false, Errors: []string{"Secret is invalid"}})
		http.Error(resp, buf.String(), http.StatusBadRequest)
		return "", errors.New("Invalid secret")
	}

	json.NewEncoder(resp).Encode(registrationResponse{Success: true, Errors: []string{}})

	return data.Name, nil
}

func getLatestAPIRegistered(resp http.ResponseWriter, req *http.Request, apiVersion string) {
	data := registrationInfoPayload{Name: apiVersion}
	json.NewEncoder(resp).Encode(data)
}
