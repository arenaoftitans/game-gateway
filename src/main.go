package main

import (
	"fmt"
	"log"
	"net/http"
	"net"
	"os"
	"strings"
	"strconv"

	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/namsral/flag"
)

type apiConnectionOptions struct {
	host     string
	port     int
	protocol string
	debug    bool
}

const redisKey = "gateway-api-latest"

func main() {
	var port int
	var redisURL string
	var defaultAPIHost string
	var apiPort int
	var apiProtocol string
	var apiDebug bool
	var allowedHostsStr string
	var allowedHosts []string
	var allowedIpsStr string
	var allowedIps []string
	var gatewaySecret string

	fs := flag.NewFlagSetWithEnvPrefix(os.Args[0], "API", 0)
	fs.StringVar(&defaultAPIHost, "host", "localhost", "default host on which to connect to the API")
	fs.IntVar(&apiPort, "port", 8181, "default port on which to connect to the API")
	fs.StringVar(&apiProtocol, "protocol", "ws", "default protocol with which to connect to the API")
	fs.BoolVar(&apiDebug, "debug", false, "print debugging info of the API")
	fs.Parse(os.Args[1:])

    flag.StringVar(&allowedHostsStr, "allowedHosts", "::1,127.0.0.1,localhost", "Host names that are allowed to register API version")
    flag.StringVar(&allowedIpsStr, "allowedIps", "::1,127.0.0.1", "IPs addresses that are allowed to register API version")
    flag.StringVar(&gatewaySecret, "secret", "", "Secret used to validate POST request")
	flag.IntVar(&port, "port", 8180, "port on which to listen")
	flag.StringVar(&redisURL, "redis", "localhost:6379", "redis address")
	flag.Parse()

	allowedHosts = strings.Split(allowedHostsStr, ",")
	allowedIps = strings.Split(allowedIpsStr, ",")

	log.Println("Will connect to API on", defaultAPIHost+":"+strconv.Itoa(apiPort), "by default")
	log.Println("Allowed hosts are: " + allowedHostsStr)
	log.Println("Allowed IPs are: " + allowedIpsStr)

	router := mux.NewRouter()

	latestAPIName := fetchFromRedis(redisURL)
	router.HandleFunc("/ws/{apiVersion}", func(resp http.ResponseWriter, req *http.Request) {
		var apiHost string
		if len(latestAPIName) > 0 {
			apiHost = latestAPIName
		} else {
			apiHost = defaultAPIHost
		}

		wsProxy(resp, req, apiConnectionOptions{host: apiHost, port: apiPort, protocol: apiProtocol, debug: apiDebug})
	}).Methods("GET")
	router.HandleFunc("/register/latest", func(resp http.ResponseWriter, req *http.Request) {
		newName, err := registerLatestAPI(resp, req, gatewaySecret)
		if err == nil {
			latestAPIName = newName
			log.Println("Updating latest API to", latestAPIName)
			saveInRedis(latestAPIName, redisURL)
		} else {
			log.Println("Failed to update latest API", err)
		}
	}).Methods("POST")
	router.HandleFunc("/register/latest", func(resp http.ResponseWriter, req *http.Request) {
		getLatestAPIRegistered(resp, req, latestAPIName)
	}).Methods("GET")
	http.Handle("/", router)

	router.Use(enforceHostHForPOSTMiddleware(allowedHosts, allowedIps))

	log.Println("Listening on", port)
	http.ListenAndServe(fmt.Sprintf(":%v", port), nil)
}

func enforceHostHForPOSTMiddleware(allowedHosts []string, allowedIps []string) func(next http.Handler) http.Handler {
    return func (next http.Handler) http.Handler {
        return http.HandlerFunc(func (w http.ResponseWriter, r *http.Request) {
            hostParts := strings.Split(r.Host, ":")
            hostName := hostParts[0]

            if r.Method != "POST" {
                next.ServeHTTP(w, r)
                return
            }

            ip, _, err := net.SplitHostPort(r.RemoteAddr)

            if err != nil {
                log.Println("Failed to parse IP, halting. Receive IP: " + r.RemoteAddr)
                http.Error(w, "Server error", http.StatusInternalServerError)
                return
            }

            log.Println("Request from IP: " + ip + " and host: " + hostName)
            if !contains(allowedHosts, hostName) || !contains(allowedIps, ip) {
                log.Println("Host " + hostName + " or IP " + ip + " is not authorised to do this.")
                http.Error(w, "Forbidden", http.StatusForbidden)
                return
            }

            next.ServeHTTP(w, r)
        })
    }
}

func contains(arr []string, str string) bool {
   for _, a := range arr {
      if a == str {
         return true
      }
   }
   return false
}

func saveInRedis(apiName string, redisURL string) {
	client := redis.NewClient(&redis.Options{
		Addr:     redisURL,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	defer func() {
		client.Close()
	}()

	_, err := client.Ping().Result()
	if err != nil {
		log.Println("Failed to connect to redis", err)
		return
	}

	setErr := client.Set(redisKey, apiName, 0).Err()
	if setErr != nil {
		log.Println("Failed to save the latest API connection info in redis", err)
		return
	}

	log.Println("Successfully saved the latest API connection info in redis")
}

func fetchFromRedis(redisURL string) string {
	client := redis.NewClient(&redis.Options{
		Addr:     redisURL,
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	defer client.Close()

	_, err := client.Ping().Result()
	if err != nil {
		log.Println("Failed to connect to redis", err)
		return ""
	}

	val, getErr := client.Get(redisKey).Result()
	if getErr != nil {
		log.Println("Failed to get latest API connection from redis", err)
		return ""
	}

	log.Printf("Retrieved %v as latest API connection from redis", val)

	return val
}
