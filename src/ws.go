package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
)

const bufferSize = 1024

// Time in seconds before a new ping is sent after a pong is received.
const pingPongDelay = 30 * time.Second

var upgrader = websocket.Upgrader{
	ReadBufferSize:  bufferSize,
	WriteBufferSize: bufferSize,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

type client struct {
	apiVersion string
	apiConn    *websocket.Conn
	clientConn *websocket.Conn
}

func (client *client) close() {
	client.apiConn.Close()
	client.clientConn.Close()
}

func (client *client) forwardToAPI() {
	defer func() {
		client.close()
	}()

	for {
		messageType, message, err := client.clientConn.ReadMessage()
		if err != nil {
			log.Println("Error while reading message from client", client.apiVersion, err)
			break
		}

		err = client.apiConn.WriteMessage(messageType, message)
		if err != nil {
			log.Println("Error while forwarding message to API", client.apiVersion, err)
			break
		}
	}
}

func (client *client) forwardToClient() {
	defer func() {
		client.close()
	}()

	for {
		messageType, message, err := client.apiConn.ReadMessage()
		if err != nil {
			log.Println("Error while reading message from API", client.apiVersion, err)
			break
		}

		err = client.clientConn.WriteMessage(messageType, message)
		if err != nil {
			log.Println("Error while forwarding message to client", client.apiVersion, err)
			break
		}
	}
}

func (client *client) setupKeepAlive() {
	keepWSConnectionAlive(client.clientConn)
	keepWSConnectionAlive(client.apiConn)
}

func keepWSConnectionAlive(conn *websocket.Conn) {
	go func() {
		<-time.After(pingPongDelay)
		conn.WriteControl(websocket.PingMessage, []byte{}, time.Time{})
		keepWSConnectionAlive(conn)
	}()
}

func wsProxy(resp http.ResponseWriter, req *http.Request, apiConnectionOptions apiConnectionOptions) {
	params := mux.Vars(req)
	apiVersion := params["apiVersion"]

	clientConn, err := upgrader.Upgrade(resp, req, nil)
	if err != nil {
		log.Println("Error while upgrading client to WS", apiVersion, err)
		return
	}

	dialer := websocket.Dialer{
		ReadBufferSize:  bufferSize,
		WriteBufferSize: bufferSize,
	}
	header := make(map[string][]string)
	for key, value := range req.Header {
		if mustCopyHeader(key) {
			header[key] = value
		}
	}
	apiURI := buildAPIURI(apiVersion, req.RequestURI, apiConnectionOptions)
	log.Println("Connecting to API ", apiURI, "for", apiVersion)
	apiConn, _, err := dialer.Dial(apiURI, header)
	if err != nil || apiConn == nil {
		log.Println("Error while connecting to API", apiVersion, err)
		return
	}

	client := &client{
		apiVersion: apiVersion,
		apiConn:    apiConn,
		clientConn: clientConn,
	}

	go client.forwardToAPI()
	go client.forwardToClient()

	go client.setupKeepAlive()

	if apiConnectionOptions.debug {
		log.Println("Setting up debug handlers")
		clientConn.SetPongHandler(func(appData string) error {
			log.Println("Client pong!")
			return nil
		})
		apiConn.SetPongHandler(func(appData string) error {
			log.Println("API pong")
			return nil
		})
	}
}

func mustCopyHeader(name string) bool {
	switch name {
	case "Connection",
		"Sec-Websocket-Key",
		"Sec-Websocket-Extensions",
		"Sec-Websocket-Version",
		"Upgrade":
		return false
	}

	return true
}

func buildAPIURI(apiVersion string, requestURI string, apiConnectionOptions apiConnectionOptions) string {
	var apiHost string

	if apiVersion == "latest" {
		apiHost = apiConnectionOptions.host
	} else {
		apiHost = apiVersion
	}

	return fmt.Sprintf(
		"%v://%v:%v%v",
		apiConnectionOptions.protocol,
		apiHost,
		apiConnectionOptions.port,
		requestURI,
	)
}
