# AoT Game Gateway

It is used a proxy to forward messages from nginx (which is directly facing the web) and the game API which are on a private docker network. For this service to work, you need to expose its port (configurable with the `PORT` env variable) with docker.

Configuration of the service is done with the following environment variables:

```bash
PORT  # Port on which the service must listen
REDIS # HOST:PORT of the redis to connect to to save the API URL.
API_HOST # Default API host (used to forward messages)
API_PORT # API port (used to forward messages)
API_PROTOCOL # API protocol (used to forward messages). Can be either ws or wss
API_DEBUG # Print ping/pong info.
```

## Usage

- To register a new version of latest, do `http POST localhost:8180/register/latest <<<'{"name": "localhost"}'`.
- To view the latest version, do `http localhost:8180/register/latest`.
- To forward messages, connect to `localhost:8180/ws/API_VERSION`.

## Requirements

- [go](https://golang.org/): the programming language
- [dep](https://golang.github.io/dep/): package manager
- [go-watcher](https://github.com/canthefason/go-watcher): if you want to rebuild and rerun the package on each change.

## Development

- Run lint: `make lint`
- Build API: `make build`
- Run API (will rebuild if anything changed): `make run`
- Watch (run & rebuilt/restart on file change): `make watch`
