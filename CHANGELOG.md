# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
### Changed
### Removed
### Fixed

## [1.4.0] – 2020-05-23
### Added
- IP validation. The IP is in fact correct: it will be the distant one of the one of the local docker interface.

## [1.3.0] – 2020-05-23
### Added
- Host validation.
### Fixed
- Don't send secret key when asking for current registration.
### Removed
- IP validation. We will only see the IP from docker's interface which will always be valid. Rely on host validation instead.

## [1.2.0] – 2020-05-23
### Added
- Enforce localhost for POST requests.
- Force caller to supply a secret to update registration.
### Changed
- Updated deps.
### Fixed
- Force a name to be passed when registering an API.

## [1.1.0] - 2018-12-13
### Added
- Can get the registered route.
- Send Pings to clients (browser and API) to keep the connection alive.
- Save latest API info in redis to retreive it on start.
### Fixed
- Correctly exit when forwarding fails.
- Don't update the latest version if a problem occured.
### Changed
- Change route pattern for registration of latest API.
- Respond with JSON when registration completes (success or error).
- Improve logging.

## [1.0.0] - 2018-03-19
### Added
- Can forward requests from client to API and from API to client.
- Can register the latest route to forward requests to the proper API.
